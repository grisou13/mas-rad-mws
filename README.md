# mas-rad-mws

# Idea

The idea is to create a weather app but using the NASA api and the weather on MARS!

The app would display the latest information on the weather on mars and allow users to browse.

It would also be able to indicate solar flares and aggregate that information in a cohesive way.

Moodboard:
- https://repository-images.githubusercontent.com/254455944/aecaaf00-81af-11ea-972b-097c76d1fe70
- https://i.pinimg.com/1200x/a5/e7/c5/a5e7c53d47658889e90d751c5d32ffbb.jpg
- https://cdn.ndtv.com/tech/images/gadgets/yahoo-weather-app-01-635.jpg

# Install


## Requirements
